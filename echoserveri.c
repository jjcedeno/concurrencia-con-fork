#include "csapp.h"
#include <unistd.h>

typedef enum { false, true } bool;

void echo(int connfd);
bool exectCmd(char* command);
void child_handler(int sgn);


int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	
	Signal(SIGCHLD, child_handler);	
	
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);

		
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		/* Determine the domain name and IP address of the client */
	
		
		if(Fork()==0){
			Close(listenfd);
			printf("server connected to %s (%s)\n", hp->h_name, haddrp);
			echo(connfd);
			Close(connfd);
			exit(0);
		}
		Close(connfd);
		
	}
	exit(0);
}


void echo(int connfd){
	size_t n;
	char buf[MAXLINE], newBuf[MAXLINE];
	rio_t rio;
	
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		strcpy(newBuf,buf);
		newBuf[strlen(newBuf)-1] = '\0';
		if (exectCmd(newBuf)){
			Rio_writen(connfd, "OK\n", 3);
		}else{
			Rio_writen(connfd, "ERROR\n", 6);
		}
	}
}

bool exectCmd(char* command){
	char *newargv[] = { NULL, NULL };
	char *newenviron[] = { NULL }; 	
	int status;
	pid_t pid;
	newargv[0] = command;	

	if ((pid = Fork()) == 0){
		if (execve(command, newargv, newenviron) < 0 ){
			raise(SIGABRT);
			exit(1);
		}
	}else {
        waitpid(pid,&status,0);
		if (status!=0){
			printf("\nComando no encontrado");
			return false;
		}else{
			return true;
		}
	}
}

void child_handler(int sgn){
	while(waitpid(-1,0,WNOHANG)>0)
		;
	return;
}
